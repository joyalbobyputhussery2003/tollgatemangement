/*
 * Click nbfs://nbhost/SystemFileSystem/Templates/Licenses/license-default.txt to change this license
 * Click nbfs://nbhost/SystemFileSystem/Templates/Classes/Class.java to edit this template
 */
package in.ac.gpckasaragod.tollgate.management.ui.data;

import java.util.Date;
import java.util.logging.Logger;

/**
 *
 * @author student
 */
public class TollType {
    private Integer id;
    private String  vehicleType;
    private     String oneWay;
    private      String twoWaY;

    public TollType(Integer id, String vehicleType, String oneWay, String twoWaY) {
        this.id = id;
        this.vehicleType = vehicleType;
        this.oneWay = oneWay;
        this.twoWaY = twoWaY;
    }
    private static final Logger LOG = Logger.getLogger(TollType.class.getName());

    public TollType(int id, String drugCode, String drugName, Double price, Date expDate, Date date) {
        throw new UnsupportedOperationException("Not supported yet."); // Generated from nbfs://nbhost/SystemFileSystem/Templates/Classes/Code/GeneratedMethodBody
    }

    public TollType(int id, String vechileType, Double oneWay, String twoWay) {
        throw new UnsupportedOperationException("Not supported yet."); // Generated from nbfs://nbhost/SystemFileSystem/Templates/Classes/Code/GeneratedMethodBody
    }

    public Integer getId() {
        return id;
    }

    public void setId(Integer id) {
        this.id = id;
    }

    public String getVehicleType() {
        return vehicleType;
    }

    public void setVehicleType(String vehicleType) {
        this.vehicleType = vehicleType;
    }

    public String getOneWay() {
        return oneWay;
    }

    public void setOneWay(String oneWay) {
        this.oneWay = oneWay;
    }

    public String getTwoWaY() {
        return twoWaY;
    }

    public void setTwoWaY(String twoWaY) {
        this.twoWaY = twoWaY;
    }
}
