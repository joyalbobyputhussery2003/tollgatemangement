/*
 * Click nbfs://nbhost/SystemFileSystem/Templates/Licenses/license-default.txt to change this license
 * Click nbfs://nbhost/SystemFileSystem/Templates/Classes/Class.java to edit this template
 */
package in.ac.gpckasaragod.tollgate.management.service.Impl;

import in.ac.gpckasaragod.tollgate.management.service.TollTypeService;
import in.ac.gpckasaragod.tollgate.management.ui.data.TollType;
import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;
import java.util.ArrayList;
import java.util.List;
import java.util.logging.Level;
import java.util.logging.Logger;

/**
 *
 * @author student
 */
public class TollTypeServiceImpl extends ConnectionServiceImpl implements TollTypeService {

    @Override
    public String saveTollType(String vehicleType, String oneWay, String twoWay) {
        try{
            Connection connection = getConnection();
            Statement statement = connection.createStatement();
            String query ="INSERT INTO TOLLGATE_MANAGEMENT(VECHILE_TYPE ,ONE_WAY,TWO_WAY) VALUES "+ "('"+vehicleType+ "','"+oneWay+"','"+twoWay+"')";
            System.err.println("Query:"+query);
            int status = statement.executeUpdate(query);
            if(status!=1){
                return "Save failed";
            }else{
                return "Saved successfully";
            }
            }catch (SQLException ex) {
         Logger.getLogger(TollTypeServiceImpl.class.getName()).log(Level.SEVERE, null, ex);
     }
     return null;
        }

    @Override
    public TollType readTollType(Integer Id) {
          TollType   tollType = null;
        try{
            Connection connection = getConnection();
            Statement statement = connection.createStatement();
            String query = "SELECT * FROM TOLLGATE_MANAGEMENT WHERE ID="+Id;
            ResultSet resultSet = statement.executeQuery(query);
            while(resultSet.next()){
                int id = resultSet.getInt("ID");
                String vechileType = resultSet.getString("VECHILE_TYPE ");
                String oneWay = resultSet.getString("ONE_WAY");
                String twoWay = resultSet.getString("TWO_WAY");
                tollType = new TollType(id, vechileType,oneWay,twoWay);
            }
            
            
        } catch (SQLException ex) {
         Logger.getLogger(TollTypeServiceImpl.class.getName()).log(Level.SEVERE, null, ex);
     }
       return tollType;
    }

    @Override
    public List<TollType> getAllTollTyp() {
          List<TollType> tolls = new ArrayList<>();
        try{
            Connection connection = getConnection();
            Statement statement = connection.createStatement();
            String query = "SELECT * FROM TOLLGATE_MANAGEMENT";
            ResultSet resultSet = statement.executeQuery(query);
            
            while(resultSet.next()){
                int id = resultSet.getInt("ID");
                String vechileType = resultSet.getString("VECHILE_TYPE");
                Double oneWay = resultSet.getDouble("ONE_WAY");
                String twoWay = resultSet.getString("TWO_WAY");
                TollType tollType = new TollType(id, vechileType,oneWay,twoWay);
                tolls.add(tollType);
            }
        } catch (SQLException ex) {
         Logger.getLogger(TollTypeServiceImpl.class.getName()).log(Level.SEVERE, null, ex);
     }
     return null;
    }

    @Override
    public String updateTollType(Integer id, String vehicleType, String oneWay, String twoWay) {
        TollType  tolls = null;
         try{
        Connection connection = getConnection();
        Statement statement = connection.createStatement();
        String query = "UPDATE TOLLGATE_MANAGEMENT SET VECHILE_TYPE='"+vehicleType+"',ONE_WAY='"+oneWay +"',TWO_WAY='"+twoWay+"';
        System.out.print(query);
        int update = statement.executeUpdate(query);
        if(update !=1)
            return "Update failed";
        else
            return "Updated successfully";
       
    }   catch (SQLException ex) {
            Logger.getLogger(TollTypeServiceImpl extends ConnectionServiceImpl implements TollTypeService {
.class.getName()).log(java.util.logging.Level.SEVERE, null, ex);
        }
        return null;
    }


    @Override
    public String deleteTollType(Integer id) {
        try{
            Connection connection =getConnection();
            String query = "DELETE FROM TOLLGATE_MANAGEMENT  WHERE ID=?";
            PreparedStatement statement = connection.prepareCall(query);
            statement.setInt(1, id);
            int delete = statement.executeUpdate();
            if (delete !=1)
                return "Delete Failed";
            else
                return "Deleted Successfully";
            
            
        } catch (Exception ex) {
            ex.printStackTrace();
            return "Deleted Failed";
    }
    }
    }
}
    

   
    

   
