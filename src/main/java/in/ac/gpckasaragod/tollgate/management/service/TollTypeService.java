/*
 * Click nbfs://nbhost/SystemFileSystem/Templates/Licenses/license-default.txt to change this license
 * Click nbfs://nbhost/SystemFileSystem/Templates/Classes/Interface.java to edit this template
 */
package in.ac.gpckasaragod.tollgate.management.service;

import in.ac.gpckasaragod.tollgate.management.ui.data.TollType;
import java.util.List;

/**
 *
 * @author student
 */
public interface TollTypeService {
     public String saveTollType(String vehicleType,String oneWay,String twoWay);
   public TollType readTollType(Integer Id);
    List<TollType> getAllTollTyp();
    public String updateTollType(Integer id,String vehicleType,String oneWay,String twoWay);
    public String deleteTollType(Integer id);
}
