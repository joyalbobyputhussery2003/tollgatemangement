/* 
 * Click nbfs://nbhost/SystemFileSystem/Templates/Licenses/license-default.txt to change this license
 * Click nbfs://nbhost/SystemFileSystem/Templates/Other/SQLTemplate.sql to edit this template
 */
/**
 * Author:  student
 * Created: 08-Jun-2022
 */

CREATE DATABASE IF NOT EXISTS TOLLGATE_MANAGEMENT;
USE TOLLGATE_MANAGEMENT;
CREATE TABLE IF NOT EXISTS TOLL_TYPE_DETAILS(ID INT PRIMARY KEY,VECHILE_TYPE VARCHAR(20) NOT NULL,ONE_WAY DECIMAL(8,2) NOT NULL,TWO_WAY DECIMAL(8,2) NOT NULL);
DESC TOLL_TYPE_DETAILS;
